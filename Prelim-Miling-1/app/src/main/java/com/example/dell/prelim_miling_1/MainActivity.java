package com.example.dell.prelim_miling_1;

import android.support.v7.app.AppCompatActivity;
import java.util.*;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;
import android.widget.*;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    Random appRandom = new Random();
    int hidRandom = appRandom.nextInt(10 - 1) + 1;

    public void onTry (View view) {

        EditText userGuess = (EditText) findViewById(R.id.GuessTxt);
        Double uGuess = Double.parseDouble(userGuess.getText().toString());

        if ("".equals(userGuess.getText().toString().trim())){
            Toast.makeText(this, "Please enter an Integer...", Toast.LENGTH_LONG).show();
            return;
        } else {
            if (uGuess > hidRandom) {
                Toast.makeText(getApplicationContext(), "Too High!", Toast.LENGTH_SHORT).show();
            } else if (uGuess < hidRandom) {
                Toast.makeText(getApplicationContext(), "Too Low!", Toast.LENGTH_SHORT).show();
            } else if (uGuess == hidRandom) {
                Toast.makeText(getApplicationContext(), "You got it right!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void onReveal (View view){
        Toast.makeText(MainActivity.this,String.valueOf(hidRandom),Toast.LENGTH_SHORT).show();
    }
}




