package com.example.dell.prelim_miling_2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick (View v) {
        String result = "";
        int triangular = 0;
        for (int i = 1; i <= 10; i++) {
                triangular += i;
                result = result + "Triangular of " + i + " = " + triangular + "\n";
            }
            TextView showTri = (TextView) findViewById(R.id.TriOutTxt);
            showTri.setText(result);
    }
}
